---
title: Sponsors
---

{% if site.sponsors.size == 0 %}
  [Be the first to sponsor {{ site.data.event.name }}!]({% link sponsors/prospectus.md %})
{% else %}
{% assign tiers = site.sponsors | tiered_sponsors: site.data.event.sponsor_tiers %}
{% for tier in tiers %}
  {% assign sponsors = tier[1] | sort: 'name' %}
  <h2>
    {{ tier[0] | capitalize }} {{ sponsors | size | pluralize: 'Sponsor' }}
  </h2>

  <ul class="sponsors row {{ tier[0] | slugify }}">
    {% for sponsor in sponsors %}
    <li class="col-xs-12 {% if tier[0] == site.data.event.sponsor_tiers[0] %}col-md-offset-3 col-md-6{% elsif tier[0] == site.data.event.sponsor_tiers[1] %}col-sm-6{% else %}col-sm-6 col-md-4{% endif %}">
      <a name="{{ sponsor['name'] | slugify }}"></a>

      <a href="{{ sponsor['site_url'] }}" class="logo">
        <img src="/images/sponsors/{{ sponsor['logo'] }}" alt="{{ sponsor['name'] }}">
      </a>

      <h4>{{ sponsor['name'] }}</h4>

      <a href="{{ sponsor['site_url'] }}">{{ sponsor['site_url'] | simplify_url }}</a>

      {{ sponsor.content }}
    </li>
    {% endfor %}
  </ul>
{% endfor %}
{% endif %}
