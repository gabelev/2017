---
title: Sponsorship Prospectus
---

## So you want to sponsor {{ site.data.event.name }}...

You're awesome!

{{ site.data.event.name }} is New York City's largest gathering of Python
developers. Brought to you by the same people who organize
[NYC Python](http://nycpython.org) and
[Learn Python NYC](http://learn.nycpython.rog), the conference draws a diverse
crowd with backgrounds in many different industries.

## Quick Facts

- The conference has sold out every year since 2014, and we expect to do so
again with over 500 attendees.

- The schedule contains two full days of talks in three tracks, on-site lunch
and break times, and an evening social event. Talks will cover topics including
core Python, data science, web development, and more. For an idea of what to
expect, take a look at
[last year’s schedule](https://2016.pygotham.org/talks/schedule/).

- We’ll be offering a Young Coders program for the first time this year. At this
event, our organizers and volunteers will teach a group of young people how to
program in Python.

- Our financial aid program is expanding. Last year, sponsors helped us grant
100 need-based and diversity scholarship tickets. This time around, we’ll also
be providing travel and lodging assistance to some.

In order to keep the registration costs as low as possible, we need your help.
Several different levels of sponsorship are available. If you have any
questions, you can contact us directly.

Once you're ready, go ahead and
send us an email to [sponsors@pygotham.org](mailto:sponsors@pygotham.org).

## Sponsorship Levels

| Benefit                            | Diamond | Platinum | Gold   | Silver | Startup |
|                                    | $15,000 | $10,000  | $6,000 | $3,000 | $1,000  |
| ---------------------------------- |:-------:|:--------:|:------:|:------:|:-------:|
| Limited to                         | 1       | 3        | 6      | ∞       | ∞        |
| Sponsor talk (subject to approval) | ✅      |          |        |        |         |
| Logo on video bumper               | ✅      |          |        |        |         |
| Booth in expo hall                 | Large   | Medium   | Small  |        |         |
| Logo in conference media           | ✅      | ✅       | ✅     | ✅     | ✅      |
| Scholarship tickets                | 4       | 3        | 2      | 1      | 1       |
| Tickets                            | 8       | 6        | 4      | 2      | 2       |
| Discount on additional tickets     | 25%     | 20%      | 15%    | 10%    | 10%     |
{: .table.table-striped.table-bordered }

## Other Forms of Sponsorship

| Benefit                                           | Social Event  | Lanyards  | Young Coders | Financial Aid |
|                                                   | $4,500*       | $2,000    | $2,500       |               |
| ------------------------------------------------- | :-----------: | :-------: | :----------: | :-----------: |
| Limited to                                        | 3             | 1         | 4            | ∞             |
| Support the next generation of Python programmers |               |           | ✅           | ✅
| Logo in conference media                          | ✅            | ✅        | ✅           | ✅            |
| Conference Tickets                                | 2             |           |              |               |
{: .table.table-striped.table-bordered }

\* Discounted social event sponsorship pricing is available for sponsors of our
main tiers.

{{ site.data.event.name }} will offer additional forms of sponsorship on top of
and in addition to the tiers included above. Details will be added as they are
finalized.

## FAQ

### Do I qualify for the startup tier?
We consider any company under three years of age with 50 or fewer employees to be
a startup.

### Why does a sponsored talk need to be approved?
All talks go through the talk review process to ensure that PyGotham remains a
high quality conference. Sponsored talks should be the same kind of talks that
you would submit through our normal call for proposals. Sales pitches and talks
that are not relevant to the Python community will be rejected.

### What are scholarship tickets?
The PyGotham financial aid program provides free tickets to attendees who would
not otherwise be able to attend. This program has helped hundreds of people
attend the conference in the past, and sponsorships make it possible. This year,
PyGotham is proud to be offering travel and lodging assistance, as well. (Want
to help with financial grants directly? Reach out!)
