---
name: Win Suen
talks:
- Spark Streaming for World Domination (and other projects)
---

I made my first foray into the NYC tech scene at an ed tech startup in 2009. Since then, I’ve taught myself to code (thanks in no small part to attending NYC Python meetups), and I’ve enjoyed every minute of the adventure. I’m now a data scientist working on inventory quality efforts at AppNexus, where I wrangle big data into submission every day. In my spare time, I love to hike, hack my garden, and pester local and state representatives about net neutrality and STEM education (do it now!).