---
name: Brendan Sudol
talks:
- Build your own "Not Hotdog" deep learning model
---

Brendan is a software engineer at 18F, an organization within the government committed to improving critical services using technology, data, & design. Before that, he worked as a data scientist for Etsy, handcrafting data tools & products for the world's largest handmade marketplace.