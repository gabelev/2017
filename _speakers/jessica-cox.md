---
name: Jessica Cox
talks:
- Tracing the flow of knowledge using Pyspark
---

Jessica Cox and Corey Harper are data scientists at Elsevier Labs, a division of the scientific publisher devoted to accelerating technology research.  Corey and Jessica have been working together this year to dig in to what they call “citing sentences”:  a collection of sentences that contain a citation within it.  Given Jessica’s past life as a biomedical scientist and Corey’s as a research librarian, together they provide a unique perspective into what features should be considered in understanding how papers are cited.  Jessica and Corey are scientometrics aficionados, believing that some of the coolest findings in science are hidden in the metadata.