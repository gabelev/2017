---
name: Steven Saporta
talks:
- Automated testing with pytest and fixtures
- Confessions of a Python packaging noob
---

Steve Saporta is a technology executive with a wide range of experience in all aspects of software engineering and business. He currently serves as Chief Technology Officer of SpinCar, a New York startup that produces interactive, photorealistic 360-degree walkarounds for automotive clients.
 
In addition to presenting at the IEEE/ACM IT Professional Conference 2015 in 2016, he has recently appeared as a panelist at the Rutgers Career Exploration and Networking Series: Computer Science & IT, and as a guest speaker to a software engineering classes at Penn State and Southern Connecticut State University.
 
A graduate of Princeton University in Computer Science, Steve enjoys skiing and playing the electric bass.