---
name: Leah Guthrie
talks:
- Finding the narrative in networks with python
---

I am a systems and computational biology PhD candidate studying how gut bacteria influence drug responses. I use python and R for data analysis. Outside of lab I focus on science education for high school students.