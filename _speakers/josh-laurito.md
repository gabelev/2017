---
name: Josh Laurito
talks:
- 'When to avoid the subway: using python to make your commute slightly less terrible'
---

Josh Laurito is the head of data engineering at [Fusion Media Group](http://thefmg.com/), which sees over 90 million visitors per month across properties like Gizmodo, The Onion, The Root and Lifehacker. Previously, Josh taught data visualization at the City University of New York and helped start [Lumesis](http://lumesis.com/), an analytics and data visualization company. Before that, he worked at Merrill Lynch and co-wrote a book on simulation modeling in finance. According to his father, for his fourth birthday he asked for an abacus. If you are interested in learning more, please visit [joshlaurito.com](http://joshlaurito.com).