---
name: Jason Owen
talks:
- Code Review, Forwards and Back
---

Sumana Harihareswara is an open source contributor and leader who has contributed to GNOME, MediaWiki, Zulip, GNU Mailman, and other FLOSS projects. She has keynoted Open Source Bridge and other open source conventions, spoken at PyCon and OSCON ([past talks](https://www.harihareswara.net/talks.html)), and manages and maintains open source projects as [Changeset Consulting](http://changeset.nyc/). She's also a stand-up comedian who's performed at Worldcon, WisCon, and AlterConf, and who serves as the charity auctioneer for the Tiptree Award. She is an alumna of [the Recurse Center](https://www.recurse.com/).

[Jason Owen](http://jasonaowen.net/) is a consultant and programmer ([Twitter](https://twitter.com/jasonaowen), [GitHub](https://github.com/jasonaowen)) who learned a tremendous amount from participating in [the Computer Action Team at Portland State University](http://cat.pdx.edu/) and from independent study at [the Recurse Center](https://www.recurse.com/). He has been conscientiously honing his skills as a code reviewer and code reviewee for several years.