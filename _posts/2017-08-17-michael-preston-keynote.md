---
title: "Announcing our first keynote: Michael Preston"
date: 2017-08-17 10:00:00 -0400
image: /assets/michael-preston.png
excerpt_separator: <!--more-->
---

Michael Preston is the Executive Director of [CSNYC](https://www.csnyc.org/), a
nonprofit that works to increase access to computer science in NYC public
schools.<!--more--> CSNYC is the city’s partner in the 10-year Computer Science
for All (CS4All) initiative and leads the national [CSforAll
Consortium](http://www.csforall.org/). Prior to joining CSNYC, he led digital
learning initiatives at the NYC Department of Education and software development
projects at Columbia University’s Center for Teaching and Learning. He earned a
Ph.D. in Cognitive Science in Education at Teachers College, Columbia
University.
