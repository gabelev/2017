---
abstract: We will discuss what ansible is and how to write a custom module for it,
  how to express config as code, and techniques for testing config as code. The talk
  will use a real-life case study (postgres administration) to create the narrative
  for why this matters and how to put these pieces together.
duration: 40
level: Intermediate
room: Madison
slot: 2017-10-06 10:45:00-04:00
speakers:
- Zach Marine
title: 'Managing Complexity: Config As Code, Custom Ansible Modules, and You'
type: talk
---

Managing complex configurations is, well, complex. Take postgres as an example: with roles, group memberships, granted privileges, and default privileges all affecting each other, keeping straight how everything fits together can feel impossible. This talk discusses an approach we took at Squarespace to simplify this chaos using a custom ansible module and config as code. Taking this problem as a case study, we'll discuss what ansible is and how to write a custom module for it, how to express config as code, and techniques for testing config as code.