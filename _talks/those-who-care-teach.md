---
abstract: 'Nothing''s better than helping someone learn a concept: regex, build tools,
  or running their first script ever! Unfortunately, teaching is hard! For many, going
  beyond show-and-tell is just too intimidating. I''ll teach you tips, tricks, & tools
  I''ve learned teaching Python to thousands of people.'
duration: 25
level: All
room: Madison
slot: 2017-10-07 13:30:00-04:00
speakers:
- Kenneth Love
title: Those Who Care, Teach!
type: talk
---

Many of us want to share our knowledge and bring people into programming or help advance people that are already here. One of the issues with this, though, is getting into teaching or mentoring is hard and a lot of us aren't sure how to do it. I can't help you get over any shyness you have but I can help you be listened to once you've reached out to people. In this talk, I'll cover ways to have a wider audience through inclusive language, positive reinforcements, and taking yourself off of any pedestals you've been placed on. I'll also cover a few tips and tricks for helping students learn what you're teaching.