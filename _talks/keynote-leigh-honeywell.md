---
abstract: Grassroots activist groups present unique and interesting security challenges.
  They are more than just their individual members, but far less defined than traditional
  organizations. What can we learn from them in terms of building more secure tools
  and systems? How can we build to protect those without a security team backing them
  up? And what lessons can those of us who work on software for more traditional organizations
  learn from the things that do work well for such groups?
duration: 45
room: PennTop South
slot: 2017-10-07 16:15:00-04:00
speakers:
- Leigh Honeywell
title: 'Halfway Between Heaven and Hell: securing grassroots groups'
type: keynote
---
