---
abstract: For all software, scaling the development process often poses a problem
  before scaling performance. Engaging more developers creates code contention and
  bugs, so all growing projects need an API for code integration. Call them extensions,
  modules, or plugins, it's time we gave them their due.
duration: 25
level: Intermediate
room: PennTop South
slot: 2017-10-07 15:40:00-04:00
speakers:
- Mahmoud Hashemi
title: A Plug for Plugins
type: talk
---

The philosophy of small, focused software has its merits, but for most of us, software is big. From your browser to your kernel, the size of these applications is lost on most of us. How do these programs even grow to this size?

In all of these cases, the answer involves an often-overlooked pattern: the plugin. Call them modules or extensions, if you'd prefer, but they are wildly successful. In fact, the only thing wider than the success of plugin-based architecture is the variety of implementations, especially in Python's dynamic environment.

This talk covers the basis for plugins, reviews Python's current offerings using examples, and provides guidance as to the bright future of plugin architecture.