---
abstract: "How do we tell how similar two words are? In spelling this can be tricky,\
  \ in pronunciation even trickier. This talk will cover some important NLP tools\
  \ (edit distance, phonetic representation, part-of-speech tagging) with the goal\
  \ of building the beginnings of a \u201Cpun generator.\u201D"
duration: 25
level: Beginner
room: PennTop North
slot: 2017-10-07 10:15:00-04:00
speakers:
- Max Schwartz
title: "Building a \u201CPun Generator\u201D in Python"
type: talk
---

The core of this talk will focus on edit distance, an important algorithm in Natural Language Processing (and Computational Biology) for determining how similar two strings are. While some of the coding involved may be beyond the “beginner level” this talk is targeted to, the goal will be to convey an understanding of how this algorithm works to even those with no prior programming experience. I will also introduce various methods of phonetic representation (writing the actual sounds of words), and touch briefly on part-of-speech tagging (identifying words as nouns, verbs, etc.). All of this will tie together to form the beginnings of a “pun generator,” although the first version of this is probably closer to a “Weird Al song title generator.” I will then talk about possible next steps for expanding this.