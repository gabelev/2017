---
abstract: "Don\u2019t settle on being just regular! In this fast-paced talk, you will\
  \ learn how to create advanced regular expressions and powerful, unconventional\
  \ nuances to using them.\n\nI will introduce little-known techniques with many code\
  \ examples, best practices and even historical origins of the name."
duration: 25
level: All
room: Madison
slot: 2017-10-06 15:10:00-04:00
speakers:
- James Cropcho
title: Avant-Garde Regular Expressions
type: talk
---

Don’t settle on being just regular! In this fast-paced talk, you will learn how to create advanced regular expressions, as well as powerful and unconventional nuances to using them.

I will introduce verbose expressions, lookahead assertions, greedy expressions, multi-line expressions, locale-aware expressions and more, with many code examples. I will introduce robust ways to use regular expressions, including named groups and returning matches as an Iterator.

I will briefly highlight multiple-expression design practices, testing, developer tooling, and even historical origins of the name.

Beginner-level regular expression familiarity is assumed. Please join me!