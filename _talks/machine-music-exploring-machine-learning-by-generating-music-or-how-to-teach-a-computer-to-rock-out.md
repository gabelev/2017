---
abstract: "Want to learn about Machine Learning and Recurrent Neural Networks? Want\
  \ to also make music? Me to! This talk is a beginners\u2019 exploration of python\
  \ machine learning frameworks to build a music generating algorithm.\n"
duration: 25
level: Beginner
room: PennTop North
slot: 2017-10-07 14:00:00-04:00
speakers:
- Gabe Levine
title: 'Machine Music: Exploring Machine Learning By Generating Music Or How To Teach
  A Computer To Rock Out'
type: talk
---

From the perspective of a musician (http://pitchfork.com/reviews/albums/12049-migration/) turned software engineer (https://github.com/gabelev), this talk will go through the ups and downs of a machine learning beginner trying to create a Recurrent Neural Network (RNN) to implement a music generating algorithm. I will build off of my PyGotham 2016 talk (The Sounds Of Data: https://www.youtube.com/watch?v=vb9c_WFMYeI) and will attempt to implement an RNN based on Alex Graves’ Generating Sequences With Recurrent Neural Networks (https://arxiv.org/pdf/1308.0850.pdf).