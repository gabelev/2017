---
abstract: During the latest season of HBO's Silicon Valley, a few characters create
  a "Shazam of food"-like app that identifies hotdogs (or not hotdogs) from photos.
  In this talk, we'll walk through the code and concepts that power this type of app
  (i.e., convolutional neural networks and transfer learning).
duration: 25
level: All
room: Madison
slot: 2017-10-06 16:25:00-04:00
speakers:
- Brendan Sudol
title: Build your own "Not Hotdog" deep learning model
type: talk
---

This talk focuses on the code and concepts behind image classification models. First, I'll break down how convolutional neural networks work. I'll present a few methods for dealing with small amounts of training data (data pre-processing and image augmentation / transformations). I'll show how to leverage (and extend) a network that's been pre-trained on a large image dataset (transfer learning). I'll use libraries like Keras and Tensorflow, and I'll show a working prototype (and open-source it) that identifies photos of pizza, because pizza (NY style of course) is way better than hotdogs :)