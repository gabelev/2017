---
abstract: 'Computing infrastructure is becoming increasingly distributed, but how
  do these systems work? This talk will introduce the Raft distributed consensus algorithm
  as implemented in Python. It will cover how Raft works, and how it solves the problem
  of replicating data across multiple nodes. '
duration: 25
level: Intermediate
room: Madison
slot: 2017-10-06 13:30:00-04:00
speakers:
- Laura Hampton
title: 'An Introduction to the Raft Distributed Consensus Algorithm '
type: talk
---

Individual computers are prone to failure, and are often unable to cope with  demands for high availability, low latency, and ability to handle large volumes of data and traffic. These demands will only increase in the future. Systems in which a consensus algorithm orchestrates a coherent group of replicated state machines have emerged as a way to meet these demands. These systems offer fault tolerance and resilience while allowing users to interact with what appears to be a single machine. 

This talk will offer a brief discussion of distributed systems and an introduction to the Raft distributed consensus algorithm, implemented in Python. Raft is used by MongoDB and Docker Swarm, among other technologies. In this talk, you will learn how Raft works, and how it solves the problems presented by replicating data across multiple machines.